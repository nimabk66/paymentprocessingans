package com.company;


import java.text.DecimalFormat;
//import java.math.RoundingMode;
//import java.util.Scanner;

//the class for all the payment funcs
public class paymentProcessing {
    //for formatting the dec numbers
    private static DecimalFormat decimalForm = new DecimalFormat("#.##");

    //the answer class for outputting the func
    public static void calc(double[] data){

        //check to see if the input is null we can add type as well but i am reading from an array so...
        if (data == null  ){
            System.out.println("there is no data in the array");
        }
        //i would use this if it was an stream i am using however because i am reading it through a casted already array there is no point
        /*
        try (Scanner scanner = new Scanner("the input stream")) {
        if (scanner.hasNextDouble()) {do the below};
        else {error out saying the stream is not providing a double value ("int can be recognized as double but we can assure this is a number")};
        }
        */


        double bal= data[0];
        //for checking the value of bal before starting
        if (bal <=0.0){
            System.out.println("the balance is negative or zero");
        }
        System.out.println("the balance is "+bal);
        //i am not sure how we are rounding so i assumed not
        //decimalForm.setRoundingMode(RoundingMode.DOWN);

        //cgoing through the stream (array here)
        for (int i=1;i<data.length;i++){
            bal-=data[i];
            System.out.println("the balance is "+bal+" now");
            // for reaching neg before end of stream i was not sure if this happens what to do so it would still invoke the other out put Change due is after this however it will tell you that you still have values in the stream
            if (bal <=0.0 && i!= data.length-1){
                System.out.println("the balance is neg or zero with additional values in the array");
                break;
            }
        }
        //outputs per instructions
        if (bal ==0.0){
            System.out.println("Balance paid in full");
        }else if (bal > 0.0){
            System.out.println("Balance due is $"+decimalForm.format(bal));
        }else{
            System.out.println("Change due is $"+decimalForm.format(-bal));
        }
    }
}

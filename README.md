﻿Payments Processing
===

An ordering system needs to process payments to take the balance of the bill down to $0.  Create an app that reads an array of numeric values from the input stream to process payments.

* The first value is a non-negative, decimal value for the balance
* Subsequent values are non-negative, decimal values, each representing a payment towards the balance
* Unlimited payments can be made but there will not be additional payments once the balance gets down to $0
* Write the following results to the output stream
  * If total payments do not take the balance down to $0, write `Balance due $X.XX`, where `X.XX` is the remaining balance to 2 decimal places
  * If the total payments take the balance down to exactly $0, write `Balance paid in full`
  * If the total payments are greater than the balance (over tendered), write `Change due $X.XX`, where `X.XX` is the change left over from the payments to 2 decimal places.

```
Ex: [50.25, 10, 40, .30] // Output: Change due $0.05
```

Requirements
---

* Fork this repository
* Add your code to your `master`
* Share your code with @kaung